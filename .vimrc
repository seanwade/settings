" VIM SETTINGS
" Sean Wade
" =================================================================
" turn on pathogen
execute pathogen#infect()

" random biz
set number
filetype plugin on
filetype plugin indent on
set showcmd
set mouse=a
syntax on
inoremap jj <esc>  " an easier input escape

" colorscheme
let macvim_skip_colorscheme=1
syntax enable
set background=dark
colorscheme solarized

" copy and paste
set clipboard=unnamed
set pastetoggle=<F2>

" Quicksave command
noremap <C-Z> :update<CR>
vnoremap <C-Z> <C-C>:update<CR>
inoremap <C-Z> <C-O>:update<CR>

" Quick quit command
noremap <Leader>e :quit<CR>  " Quit current window
noremap <Leader>E :qa!<CR>   " Quit all windows

" remap the leader key
let mapleader = ","

" easier moving between tabs
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>

" bind Ctrl+<movment> keys to move the windows
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" map sort function to a key
vnoremap <Leader>s :sort<CR>o

" easier moving of code blocks
vnoremap < <gv
vnoremap > >gv

" useful settings
set history=700
set undolevels=700

" TAB settings
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab

" search settings
set hlsearch
set incsearch
set ignorecase
set smartcase

" Removes highlight of your last search
noremap <C-n> :nohl<CR>
vnoremap <C-n> :nohl<CR>
inoremap <C-n> :nohl<CR>

" Insert blank lines without going into insert mode
nmap t o<ESC>k
nmap T O<ESC>j



" Plugin Settings
" ================================================
"" Settings for vim-powerline
set laststatus=2

" " Settings for ctrlp
let g:ctrlp_max_height = 30
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=*/coverage/*

" " Settings for jedi-vim
let g:jedi#usages_command = "<leader>z"
let g:jedi#popup_on_dot = 1
let g:jedi#popup_select_first = 1
map <Leader>b Oimport ipdb; ipdb.set_trace() # BREAKPOINT<C-c>

" " Better navigating through omnicomplete option list
" " See
" http://stackoverflow.com/questions/2170023/how-to-map-keys-for-popup-menu-in-vim
set completeopt=longest,menuone
function! OmniPopup(action)
    if pumvisible()
        if a:action == 'j'
            return "\<C-N>"
        elseif a:action == 'k'
           return "\<C-P>"
        endif
    endif
    return a:action
endfunction

inoremap <silent><C-j> <C-R>=OmniPopup('j')<CR>
inoremap <silent><C-k> <C-R>=OmniPopup('k')<CR>

" " Python folding
" " mkdir -p ~/.vim/ftplugin
" " wget -O ~/.vim/ftplugin/python_editing.vim
" http://www.vim.org/scripts/download_script.php?src_id=5492
set nofoldenable
